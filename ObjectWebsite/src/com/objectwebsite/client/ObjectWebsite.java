package com.objectwebsite.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.objectwebsite.client.service.ClientImplementor;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ObjectWebsite implements EntryPoint {
		
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		ClientImplementor clientImplementor = new ClientImplementor(GWT.getModuleBaseURL() + "service");
		RootPanel.get().add(clientImplementor.getMainGUI());
	}
	
}
