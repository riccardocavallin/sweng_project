package com.objectwebsite.client.gui;

import com.objectwebsite.client.model.Utente;

public interface GuiInterface {

	
	void updateGui (Utente sessionUser);
	
	void updateStatusLabel(String txt);
	
}
