package com.objectwebsite.client.gui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.objectwebsite.client.model.Utente;
import com.objectwebsite.client.service.ClientImplementor;

public class Login extends Composite implements GuiInterface{

	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(3, 2);
	private TextBox nicknameTextBox;
	private PasswordTextBox passwordTextBox;
	private Main main;

	private ClientImplementor clientImplementor;

	public Login(ClientImplementor clientImplementor) {
		initWidget(this.vPanel);
		this.clientImplementor = clientImplementor;

		
		Label nickname = new Label("Nickname: ");
		nicknameTextBox = new TextBox();
		nicknameTextBox.setStyleName("form-control");
		nicknameTextBox.getElement().setPropertyString("placeholder", "Nickname");
		gridPanel.setWidget(0, 0, nickname);
		gridPanel.setWidget(0, 1, nicknameTextBox);

		Label password = new Label("Password: ");
		passwordTextBox = new PasswordTextBox();
		passwordTextBox.setStyleName("form-control");
		passwordTextBox.getElement().setPropertyString("placeholder", "Password");
		gridPanel.setWidget(1, 0, password);
		gridPanel.setWidget(1, 1, passwordTextBox);

		
		vPanel.add(gridPanel);
		
		Button loginBTN = new Button("Login", new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String username = nicknameTextBox.getText().trim();
				String password = passwordTextBox.getText().trim();
				
				if(username.equals("admin") && password.equals("admin")) {
					clientImplementor.getMainGUI().setAdmin();
				} else {
				
					clientImplementor.login(username, password);
				
				}
				resetLabels();
				
			}
		});
		vPanel.add(loginBTN);
	}

	private void resetLabels() {
		nicknameTextBox.setText("");
		passwordTextBox.setText("");
	}


	@Override
	public void updateGui(Utente sessionUser) {
		resetLabels();
	}

	@Override
	public void updateStatusLabel(String txt) {
		Window.alert(txt);
		
	}

}


