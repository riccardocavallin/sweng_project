package com.objectwebsite.client.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import org.mapdb.Atomic.Var;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ShowRangeEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.objectwebsite.client.service.ClientImplementor;
import com.objectwebsite.client.gui.ProfiloUtente.ButtonClickHandler;
import com.objectwebsite.client.model.*;

public class Main extends Composite {

	private VerticalPanel enclosurePanel = new VerticalPanel();
	private VerticalPanel contentPanel = new VerticalPanel();
	
	public static final String register_BTN = "registrazione";
	public static final String login_BTN = "login";
	public static final String profile_BTN = "profile";
	public static final String main_BTN = "main";
	
	private boolean isAdmin = false;
	
	public UtenteRegistrato sessionUser = null;
	public ProfiloUtente userProfile = null;
	public ProfiloAdmin admin = null;
	
	private GuiInterface gui = null;
	private Registration registration = null;
	private Login login = null;

	private ClientImplementor clientImplementor; 
	
	private ArrayList<Oggetto> oggetti; 
	private ArrayList<Domanda> domande; 
	private ArrayList<Risposta> risposte; 
	
	/**
	 * Costruttore che inizializza alcuni elementi nel db
	 * @param clientImplementor oggetto per effettuare le RPC
	 */
	public Main(ClientImplementor clientImplementor) {
		initWidget(this.enclosurePanel); 
		enclosurePanel.add(contentPanel);
		this.clientImplementor = clientImplementor;
		
		// inizializzazione dei contenuti nel db
		clientImplementor.nuovaCategoria(new Categoria("Abbigliamento", ""));
		clientImplementor.nuovaCategoria(new Categoria("Giardinaggio", ""));
		clientImplementor.nuovaCategoria(new Categoria("Sport", ""));
		clientImplementor.nuovaCategoria(new Categoria("Elettronica", ""));
		clientImplementor.nuovaCategoria(new Categoria("Casa", ""));
		clientImplementor.registration(new UtenteRegistrato("ric", "ric", "riccardo", "cavallin", 3456173, "cvlrcr379nsu1", "via jacopo della quercia 1", "riccardo@cavallin.com"));
		clientImplementor.registration(new UtenteRegistrato("marty", "marty", "martino", "simonetti", 3456173, "cvlrcr379nsu1", "via jacopo della quercia 1", "riccardo@cavallin.com"));
		setUpDisplay();
	}
	
	public void doRegistration() {
		if (registration == null) {
			registration = new Registration(clientImplementor);
		}
		displayThis(registration);
	} 
	
	public void doLogin() {
		login = new Login(clientImplementor); 
		displayThis(login);
	}
	
	public void doProfile() {
		userProfile = new ProfiloUtente(clientImplementor, sessionUser);
		gui = userProfile;
		displayThis(userProfile);
	}
	
	public void doLogout() {
		sessionUser = null;
		isAdmin = false;
		setUpDisplay();
	}
	
	/**
	 * Pagina iniziale del login
	 */
	private void setUpDisplay() {
		if (contentPanel.getWidgetCount()>0) {
			for (int i = 0; i < contentPanel.getWidgetCount(); i++) {
				contentPanel.remove(i);
			}
		}
		if (isAdmin) {
			
			admin = new ProfiloAdmin(clientImplementor);
			gui = admin;
			displayThis(admin);
			
		} else if (sessionUser == null)  {
			HorizontalPanel hPanel = new HorizontalPanel();

			Button registerBTN = new Button("Registrati");
			registerBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
			registerBTN.setTitle(register_BTN);
			registerBTN.addClickHandler(new ButtonClickHandler());
			hPanel.add(registerBTN);

			Button loginBTN = new Button("Login");
			loginBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
			loginBTN.setTitle(login_BTN);
			loginBTN.addClickHandler(new ButtonClickHandler());
			hPanel.add(loginBTN);
			
			clientImplementor.getOggetti();
			
			contentPanel.add(hPanel);
		} else { //bottone per visualizzare dati del profilo
			doProfile();
		}
	}
	
	/**
	 * Rimozione di tutti i contenuti visualizzati per poterne visualizzare uno nuovo
	 * @param toDisplay oggetto da visualizzare
	 */
	private void displayThis(Composite toDisplay) {
		contentPanel.clear();
		// aggiunge il nuovo elemento daVisualizzare visualizzare
		if (toDisplay != null)
		contentPanel.add(toDisplay);
	}
	
	public void loginUser(UtenteRegistrato result) {
		sessionUser = result;
		isAdmin = false;
		setUpDisplay();
		
	}
	
	public void setAdmin() {
		isAdmin = true;
		setUpDisplay();
	}
	
	/**
	 * Classe per la gestione dei click sui diversi bottoni
	 */
	public class ButtonClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			Button clicked = (Button) event.getSource();
			switch(clicked.getTitle()) {
				case register_BTN: 
					doRegistration();
					break;
				case login_BTN: 
					doLogin();
					break;
				case main_BTN: 
					contentPanel.clear();
					setUpDisplay();
					break;
			}
			
		}
		
	}
	
	public void setCategorie(ArrayList<Categoria> categorie) {
		if (gui == userProfile) {
			userProfile.setCategorie(categorie);
		} else if (gui == admin) {
			admin.setCategorie(categorie);
		}
	}
	
	public void setOggetti(ArrayList<Oggetto> oggetti) {
		if (sessionUser == null && !isAdmin) {
			this.oggetti = oggetti;
			showOggetti();

		} else if (gui == userProfile) {
			userProfile.setOggetti(oggetti);;
		} else if (gui == admin) {
			admin.setOggetti(oggetti);
		} 
	}

	/**
	 * Ordinamento e visualizzazione di tutti gli oggetti con asta in corso
	 */
	public void showOggetti() {
		
		// ordinamento degli oggetti in base alla data di scadenza
		Collections.sort(oggetti, new Comparator<Oggetto>() {
			public int compare(Oggetto o1, Oggetto o2) {
				return o1.getScadenzaAsta().compareTo(o2.getScadenzaAsta());
			}
		});
		
		contentPanel.add(new Label("Oggetti atttualemente all'asta: "));
		Grid gridOggetti = new Grid(oggetti.size(), 2);
		for (int i = 0; i < oggetti.size(); i++) {
			if (oggetti.get(i).getScadenzaAsta().after(new Date())) {
				int k = i;
				gridOggetti.setWidget(i, 0, new Label(oggetti.get(i).getNome()));
				Button vediBTN = new Button("Vedi");
				vediBTN.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						showObjPage(k, oggetti);
					}
				});
				gridOggetti.setWidget(i, 1, vediBTN);
			}		
		}
		contentPanel.add(gridOggetti);
	}
	
	/**
	 * Visualizzazione pagina dell'oggetto
	 * @param obj indice nel vettore dell'oggetto
	 * @param oggetti vettore di oggetti
	 */
	private void showObjPage(int obj, ArrayList<Oggetto> oggetti) {
		contentPanel.clear();
		contentPanel.add(new Label("Nome: " + oggetti.get(obj).getNome()));
		contentPanel.add(new Label("Descrizione: " + oggetti.get(obj).getDescrizione()));
		contentPanel.add(new Label("Prezzo: " + oggetti.get(obj).getPrezzoBase()));
		contentPanel.add(new Label("Categoria: " + oggetti.get(obj).getCategoria().getNome()));
		if (oggetti.get(obj).getUltimaOfferta() != null) {
			contentPanel.add(new Label("Ultima offerta: " + oggetti.get(obj).getUltimaOfferta().getImporto()));
		}
		contentPanel.add(new Label("Scadenza asta: " + oggetti.get(obj).getScadenzaAsta()));
		contentPanel.add(new Label("Venditore: " + oggetti.get(obj).getVenditore().getNickname()));	
		clientImplementor.getDomande(oggetti.get(obj));
		
	}
	
	public void setDomande(ArrayList<Domanda> domande) {
		this.domande = domande;
		if (sessionUser == null && !isAdmin) {
			clientImplementor.getRisposte();
		} else if (gui == userProfile) {
			userProfile.setDomande(domande);;
		} else if (gui == admin) {
			admin.setDomande(domande);
		}
	}
	
	public void setRisposte(ArrayList<Risposta> risposte) {
		this.risposte = risposte;
		if (sessionUser == null && !isAdmin) {
			showDomandeRisposte();
		} else if (gui == admin) {
			admin.setRisposte(risposte);
		}
	}
	
	/**
	 * Visualizzazione della tabella con domande e risposte
	 */
	private void showDomandeRisposte() {
		if (domande.size() == 0) {
			contentPanel.add(new Label("Questo oggetto non ha nessuna domanda!"));
		} else {
			FlexTable flexTable = new FlexTable();
			flexTable.setStyleName("flexTable");
			flexTable.setText(0, 0, "Domanda");
			flexTable.setText(0, 1, "Risposta");
			for (int i = 0; i < domande.size(); i++) {
				flexTable.setText(i+1, 0, domande.get(i).getTesto());
				for (int k = 0; k < risposte.size(); k++) {
					if (risposte.get(k).getDomanda().getId() == domande.get(i).getId()) {
						flexTable.setText(i+1, 1, risposte.get(k).getTesto());
					}
				}
			}
			contentPanel.add(flexTable);
		}
		
		Button mainBTN = new Button("Torna alla home");
		mainBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		mainBTN.setTitle(main_BTN);
		mainBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(mainBTN);	
	}
	
	public void updateStatusLabel(String txt) {
		gui.updateStatusLabel(txt);
	}

}
