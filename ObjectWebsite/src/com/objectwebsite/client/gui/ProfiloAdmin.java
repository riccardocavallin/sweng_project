package com.objectwebsite.client.gui;

import java.util.ArrayList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.objectwebsite.client.model.Categoria;
import com.objectwebsite.client.model.Domanda;
import com.objectwebsite.client.model.Offerta;
import com.objectwebsite.client.model.Oggetto;
import com.objectwebsite.client.model.Risposta;
import com.objectwebsite.client.model.Utente;
import com.objectwebsite.client.service.ClientImplementor;

public class ProfiloAdmin extends Composite implements GuiInterface {


	private Grid gridPanel = new Grid(3, 2);
	private TextBox nomeTextBox;
	private TextBox padreTextBox;
	private ClientImplementor clientImplementor;
	private String nuovaCat;

	private ArrayList<Oggetto> oggetti;
	private ArrayList<Categoria> categorie;
	private ArrayList<Domanda> domande;
	private ArrayList<Risposta> risposte;
	private ArrayList<Offerta> offertePerOggetto;

	private VerticalPanel enclousurePanel = new VerticalPanel();
	private VerticalPanel contentPanel = new VerticalPanel();

	public static final String nuovaCategoria_BTN = "nuovaCategoria";
	public static final String tornaProfilo_BTN = "torna";
	public static final String gestisci_BTN = "gestisci";
	public static final String nomeCat_BTN = "nomeCat";
	public static final String logout_BTN = "logout";

	private int var;

	public ProfiloAdmin(ClientImplementor clientImplementor) {

		initWidget(this.enclousurePanel);
		this.clientImplementor = clientImplementor;
		
		mainScreen();
	}

	/**
	 * Pagina principale per l'admin
	 */
	private void mainScreen() {
		
		Button newCategoriaBTN = new Button("Nuova categoria");
		newCategoriaBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		newCategoriaBTN.setTitle(nuovaCategoria_BTN);
		newCategoriaBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(newCategoriaBTN);

		Button aggiornaCatBTN = new Button("Cambia nome categoria");
		aggiornaCatBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		aggiornaCatBTN.setTitle(nomeCat_BTN);
		aggiornaCatBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(aggiornaCatBTN);
		
		Button gestisciOggettiBTN = new Button("Gestisci oggetti e offerte");
		gestisciOggettiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		gestisciOggettiBTN.setTitle(gestisci_BTN);
		gestisciOggettiBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(gestisciOggettiBTN);
		
		Button logoutBTN = new Button("Logout");
		logoutBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		logoutBTN.setTitle(logout_BTN);
		logoutBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(logoutBTN);

		enclousurePanel.add(contentPanel);
	}
	
	/**
	 * Classe per la gestione dei click sui diversi bottoni
	 */
	public class ButtonClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			Button clicked = (Button) event.getSource();
			switch(clicked.getTitle()) {
			case nuovaCategoria_BTN: 
				var = 1;
				clientImplementor.getCategorie();
				contentPanel.clear();
				break;
			case tornaProfilo_BTN:
				contentPanel.clear();
				mainScreen();
				break;
			case nomeCat_BTN:
				var = 2;
				clientImplementor.getCategorie();
				contentPanel.clear();
				break;
			case logout_BTN:
				clientImplementor.getMainGUI().doLogout();
				break;
			case gestisci_BTN:
				clientImplementor.getOggetti();
				break;

			}

		}

	}
	
	/**
	 * Inserimento di una nuova categoria
	 */
	public void doNuovaCategoria() {

		Label lbl0 = new Label("Nome: ");
		nomeTextBox = new TextBox();
		nomeTextBox.setStyleName("form-control");
		nomeTextBox.getElement().setPropertyString("placeholder", "Nome");
		gridPanel.setWidget(0, 0, lbl0);
		gridPanel.setWidget(0, 1, nomeTextBox);

		Label lbl1 = new Label("Categoria padre: ");
		ListBox listBox = new ListBox();
		listBox.addItem("Nessun padre");
		for (int i = 0; i < categorie.size(); i++) {
			listBox.addItem(categorie.get(i).getNome());
		}
		listBox.setVisibleItemCount(categorie.size()/2);
		gridPanel.setWidget(1, 0, lbl1);
		gridPanel.setWidget(1, 1, listBox);


		Button inserisciBTN = new Button("Inserisci categoria", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				nuovaCat = nomeTextBox.getText().trim();
				String padre = listBox.getSelectedItemText();                        

				clientImplementor.getCategoria(padre);

				nomeTextBox.setText("");
				padreTextBox.setText("");

			}
		});  
		gridPanel.setWidget(2, 0, inserisciBTN);

		contentPanel.add(gridPanel);
		enclousurePanel.add(contentPanel);
		addExitBTN();

	}


	public void setCategoria(Categoria result){  
		if (result == null) {
			Categoria cat = new Categoria(nuovaCat, "");
			clientImplementor.nuovaCategoria(cat);
		} else {
			Categoria cat = new Categoria(nuovaCat, result.getNome());
			clientImplementor.nuovaCategoria(cat);
		}
		 

	}
	
	/**
	 * Riceve il vettore dalla RPC e lo setta
	 * Poi chiama il metodo giusto in base al contesto
	 * @param categorie arraylist di categorie
	 */
	public void setCategorie(ArrayList<Categoria> categorie) {
		this.categorie = categorie;
		switch (var) {
		case 1:
			doNuovaCategoria();
			break;
		case 2:
			aggiornaCategoria();
			break;
		}


	}
	
	/**
	 * Riceve il vettore dalla RPC e lo setta
	 * @param oggetti arraylist di oggetti
	 */
	public void setOggetti(ArrayList<Oggetto> oggetti) {
		this.oggetti = oggetti;
		showOggetti();
	}
	
	/**
	 * Visualizzazione di tutti fli oggetti e rispettivi bottoni
	 */
	private void showOggetti() {
		contentPanel.clear();
		Grid gridOggetti = new Grid(oggetti.size(), 4);
		for (int i = 0; i < oggetti.size(); i++) {
			int k = i;
			gridOggetti.setWidget(i, 0, new Label(oggetti.get(i).getNome()));
			Button vediBTN = new Button("Vedi");
			vediBTN.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					contentPanel.clear();
					contentPanel.add(new Label("Nome: " + oggetti.get(k).getNome()));
					clientImplementor.getDomande(oggetti.get(k));
				}
			});
			gridOggetti.setWidget(i, 1, vediBTN);
			
			Button eliminaBTN = new Button("Elimina");
			eliminaBTN.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					contentPanel.clear();
					clientImplementor.eliminaOggetto(oggetti.get(k));
					clientImplementor.getOggetti();
				}
			});
			gridOggetti.setWidget(i, 2, eliminaBTN);
			
			Button offerteBTN = new Button("Offerte");
			offerteBTN.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					contentPanel.clear();
					clientImplementor.getOffertePerOggetto(oggetti.get(k));
				}
			});
			gridOggetti.setWidget(i, 3, offerteBTN);

		}
		contentPanel.add(gridOggetti);
		addExitBTN();
	}
	
	public void addExitBTN() {
		Button tornaProfiloBTN = new Button("Torna alla pagina profilo");
		tornaProfiloBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaProfiloBTN.setTitle(tornaProfilo_BTN);
		tornaProfiloBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(tornaProfiloBTN);
	}
	
	/**
	 * Setter di domande dalla RPC
	 * @param domande arraylist di domande
	 */
	public void setDomande(ArrayList<Domanda> domande) {
		this.domande = domande;
		clientImplementor.getRisposte();
	}
	
	/**
	 * Setter di risposte dalla RPC
	 * @param risposte arraylist di risposte
	 */
	public void setRisposte(ArrayList<Risposta> risposte) {
		this.risposte = risposte;
		showDomandeRisposte();
	}
	
	/**
	 * Visualizzazione tramite flextable di domande e risposte
	 */
	private void showDomandeRisposte() {
		if (domande.size() == 0) {
			contentPanel.add(new Label("Questo oggetto non ha nessuna domanda!"));
		} else {
			FlexTable flexTable = new FlexTable();
			flexTable.setStyleName("flexTable");
			flexTable.setText(0, 0, "Domanda");
			flexTable.setText(0, 1, "Elimina Doamnda");
			flexTable.setText(0, 2, "Risposta");
			flexTable.setText(0, 3, "Elimina Risposta");
			for (int i = 0; i < domande.size(); i++) {
				int indexD = i;
				Button eliminaDomandaBTN = new Button("Elimina");
				eliminaDomandaBTN.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						clientImplementor.eliminaDomanda(domande.get(indexD));
						clientImplementor.getOggetti();
						
					}
				});
				flexTable.setText(i+1, 0, domande.get(i).getTesto());
				flexTable.setWidget(i+1, 1, eliminaDomandaBTN);
				for (int k = 0; k < risposte.size(); k++) {
					if (risposte.get(k).getDomanda().getId() == domande.get(i).getId()) {
						int indexR = k;
						Button eliminaRispostaBTN = new Button("Elimina");
						eliminaRispostaBTN.addClickHandler(new ClickHandler() {
							
							@Override
							public void onClick(ClickEvent event) {
								clientImplementor.eliminaRisposta(risposte.get(indexR));
								clientImplementor.getOggetti();
								
							}
						});
						flexTable.setText(i+1, 2, risposte.get(k).getTesto());
						flexTable.setWidget(i+1, 3, eliminaRispostaBTN);
					}
				}
			}
			contentPanel.add(flexTable);
		}
		
		addExitBTN();
	}
	
	/**
	 * Aggiornamento di una categoria
	 */
	public void aggiornaCategoria() {

		Label lbl0 = new Label("Nuovo nome: ");
		nomeTextBox = new TextBox();
		nomeTextBox.setStyleName("form-control");
		nomeTextBox.getElement().setPropertyString("placeholder", "Nome");
		gridPanel.setWidget(1, 0, lbl0);
		gridPanel.setWidget(1, 1, nomeTextBox);

		Label lbl1 = new Label("Vecchio nome: ");
		ListBox listBox = new ListBox();
		for (int i = 0; i < categorie.size(); i++) {
			listBox.addItem(categorie.get(i).getNome());
		}
		listBox.setVisibleItemCount(categorie.size()/2);
		gridPanel.setWidget(0, 0, lbl1);
		gridPanel.setWidget(0, 1, listBox);


		Button inserisciBTN = new Button("Aggiorna nome", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String nomeNuovo = nomeTextBox.getText().trim();
				String nomeVecchio = listBox.getSelectedItemText();
				clientImplementor.aggiornaCategoria(nomeVecchio, nomeNuovo);
			}
		});  
		gridPanel.setWidget(2, 0, inserisciBTN);

		contentPanel.add(gridPanel);
		enclousurePanel.add(contentPanel);
		addExitBTN();
	}
	
	/**
	 * Setter di offertePerOggetto dalla RPC
	 * @param offertePerOggetto arraylist di offerte
	 */
	public void setOffertePerOggetto(ArrayList<Offerta> offertePerOggetto) {
		this.offertePerOggetto = offertePerOggetto;
		eliminaOfferte();
	}
	
	/**
	 * Visualizzazione ed eliminazione delle offerte
	 */
	public void eliminaOfferte() {
		FlexTable flexTable = new FlexTable();
		flexTable.setStyleName("flexTable");
		if (offertePerOggetto != null && offertePerOggetto.size() > 0) {
			contentPanel.add(new Label(offertePerOggetto.get(0).getOggetto().getNome()));
			flexTable.setText(0, 0, "Offerente");
			flexTable.setText(0, 1, "Importo");
			flexTable.setText(0, 2, "Elimina");
			for (int i = 0; i < offertePerOggetto.size(); i++) {
				int k = i;
				flexTable.setText(i+1, 0, offertePerOggetto.get(i).getOfferente().getNickname());
				flexTable.setText(i+1, 1, offertePerOggetto.get(i).getImporto().toString());
				Button eliminaOffertaBTN = new Button("Elimina");
				eliminaOffertaBTN.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						contentPanel.clear();
						clientImplementor.eliminaOfferta(offertePerOggetto.get(k));;
						clientImplementor.getOffertePerOggetto(offertePerOggetto.get(k).getOggetto());
						
					}
				});
				flexTable.setWidget(i+1, 2, eliminaOffertaBTN);
			}
		} else if (offertePerOggetto.size() == 0) {
			flexTable.setText(0, 0, "Non c'è nessuna offerta per l'oggetto selezionato!");
		}
		contentPanel.add(flexTable);
		addExitBTN();
		
	}


	@Override
	public void updateGui(Utente sessionUser) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateStatusLabel(String txt) {
		Window.alert(txt);  

	}

}