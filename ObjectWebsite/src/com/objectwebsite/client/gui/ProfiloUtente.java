package com.objectwebsite.client.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import com.google.gwt.user.client.Window;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.objectwebsite.client.model.Categoria;
import com.objectwebsite.client.model.Domanda;
import com.objectwebsite.client.model.Offerta;
import com.objectwebsite.client.model.Oggetto;
import com.objectwebsite.client.model.Risposta;
import com.objectwebsite.client.model.Utente;
import com.objectwebsite.client.model.UtenteRegistrato;
import com.objectwebsite.client.service.ClientImplementor;

public class ProfiloUtente extends Composite implements GuiInterface {
	
	private ClientImplementor clientImplementor;
	private UtenteRegistrato user;
	private VerticalPanel enclousurePanel = new VerticalPanel();
	private VerticalPanel contentPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(6, 2);
	private TextBox nomeTextBox;
	private TextBox descrizioneTextBox;
	private TextBox prezzoBaseTextBox;
	private TextBox categoriaTexBox;
	private DateBox dateBox;
	
	public static final String vendi_BTN = "vendi";
	public static final String dati_BTN = "dati";
	public static final String tornaProfilo_BTN = "torna";
	public static final String oggetti_BTN = "oggetti";
	public static final String offri_BTN = "offri";
	public static final String domanda_BTN = "domanda";
	public static final String logout_BTN = "logout";
	
	private ArrayList<Oggetto> oggetti = null;
	private ArrayList<Categoria> categorie;	
	private ArrayList<Domanda> domande;
	private ArrayList<Categoria> sottoCategorie;
	private ArrayList<Oggetto> oggettiCategorie;
	private String categoriaAttuale = "Nessuna categoria";
	private int pos;
	private int var;
	private int var2;
	double importo = 0; 
	
	public ProfiloUtente(ClientImplementor clientImplementor, UtenteRegistrato user) {
		
		initWidget(this.enclousurePanel);
		this.clientImplementor = clientImplementor;
		this.user = user;
		mainScreen();
	}
	
	/**
	 * Visualizzazione schermata principale per l'utente
	 */
	private void mainScreen() {
	
		Button datiBTN = new Button("Mostra dati");
		datiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		datiBTN.setTitle(dati_BTN);
		datiBTN.addClickHandler(new ButtonClickHandler());
		
		Button vendiBTN = new Button("Vendi un oggetto");
		vendiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		vendiBTN.setTitle(vendi_BTN);
		vendiBTN.addClickHandler(new ButtonClickHandler());
		
		Button oggettiBTN = new Button("Guarda gli oggetti in vendita");
		oggettiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		oggettiBTN.setTitle(oggetti_BTN);
		oggettiBTN.addClickHandler(new ButtonClickHandler());
		
		Button logoutBTN = new Button("Logout");
		logoutBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		logoutBTN.setTitle(logout_BTN);
		logoutBTN.addClickHandler(new ButtonClickHandler());
		
		contentPanel.add(datiBTN);
		contentPanel.add(vendiBTN);
		contentPanel.add(oggettiBTN);
		contentPanel.add(logoutBTN);
		enclousurePanel.add(contentPanel);

	}
	
	/**
	 * Classe per la gestione dei click sui diversi bottoni
	 */
		public class ButtonClickHandler implements ClickHandler {

			@Override
			public void onClick(ClickEvent event) {
				Button clicked = (Button) event.getSource();
				switch(clicked.getTitle()) {
					case vendi_BTN: 
						contentPanel.clear();
						var2 = 1;
						clientImplementor.getCategorie();
						break;
					case dati_BTN: 
						contentPanel.clear();
						showDati();
						break;
					case tornaProfilo_BTN:
						contentPanel.clear();
						mainScreen();
						break;
					case oggetti_BTN:
						contentPanel.clear();
						var = 1; 
						clientImplementor.getOggetti();
						break;
					case offri_BTN:
						doOfferta(oggetti.get(pos));
						break;
					case domanda_BTN:
						doDomanda(oggetti.get(pos));
						break;
					case logout_BTN:
						clientImplementor.getMainGUI().doLogout();
						break;
				}
				
			}
			
		}
	
	/**
	 * Visualizzazione schermata di inserimento per un oggetto
	 */
	private void showInserimento() {
		
		Label lbl0 = new Label("Nome: ");
		nomeTextBox = new TextBox();
		nomeTextBox.setStyleName("form-control");
		nomeTextBox.getElement().setPropertyString("placeholder", "Nome");
		gridPanel.setWidget(0, 0, lbl0);
		gridPanel.setWidget(0, 1, nomeTextBox);
	
		Label lbl1 = new Label("Descrizione: ");
		descrizioneTextBox = new TextBox();
		descrizioneTextBox.setStyleName("form-control");
		descrizioneTextBox.getElement().setPropertyString("placeholder", "Descrizione");
		gridPanel.setWidget(1, 0, lbl1);
		gridPanel.setWidget(1, 1, descrizioneTextBox);
	
		Label lbl2 = new Label("Prezzo Base: ");
		prezzoBaseTextBox = new TextBox();
		prezzoBaseTextBox.setStyleName("form-control");
		prezzoBaseTextBox.getElement().setPropertyString("placeholder", "Prezzo Base");
		gridPanel.setWidget(2, 0, lbl2);
		gridPanel.setWidget(2, 1, prezzoBaseTextBox);
	
		Label lbl3 = new Label("Categoria: ");
		ListBox listBox = new ListBox();
		for (int i = 0; i < categorie.size(); i++) {
			listBox.addItem(categorie.get(i).getNome());
		}
		listBox.setVisibleItemCount(categorie.size()/2);
		gridPanel.setWidget(3, 0, lbl3);
		gridPanel.setWidget(3, 1, listBox);
	
		Label lbl4 = new Label("Scadenza: ");
		DateTimeFormat dateFormat = DateTimeFormat.getLongDateFormat();
		dateBox = new DateBox();
		dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
		dateBox.getDatePicker().setYearArrowsVisible(true);
		dateBox.addValueChangeHandler(new ValueChangeHandler<Date>() {		
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();
				String dateString = DateTimeFormat.getMediumDateFormat().format(date);
			}
		});
		dateBox.setValue(new Date(), true);
		gridPanel.setWidget(4, 0, lbl4);
		gridPanel.setWidget(4, 1, dateBox);
		
		Button inserisciBTN = new Button("Inserisci Oggetto", new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String nome = nomeTextBox.getText().trim();
				String descrizione = descrizioneTextBox.getText().trim();
				double prezzoBase = Integer.parseInt(prezzoBaseTextBox.getText().trim());
				String nomeCategoria = listBox.getSelectedItemText();
				Date dataScadenza = (Date) dateBox.getValue();
				
				if (!dataScadenza.after(new Date())) {
					Window.alert("La data inserita non è valida");
					resetLabels();
					
				}
				int id = new Date().hashCode();
				Oggetto nuovoOggetto = new Oggetto(nome, descrizione, prezzoBase, new Categoria(nomeCategoria, null), dataScadenza, user, id);
				clientImplementor.nuovoOggetto(nuovoOggetto);
				
				resetLabels();
				
			}
		});	
		gridPanel.setWidget(5, 0, inserisciBTN);
		
		Button tornaProfiloBTN = new Button("Torna alla pagina profilo");
		tornaProfiloBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaProfiloBTN.setTitle(tornaProfilo_BTN);
		tornaProfiloBTN.addClickHandler(new ButtonClickHandler());
		
		contentPanel.add(gridPanel);
		contentPanel.add(tornaProfiloBTN);
		enclousurePanel.add(contentPanel);
	}
	
	/**
	 * Reset di tutte le etichette di inserimento
	 */
	private void resetLabels() {
		// ripristina i valori di default delle etichette
		nomeTextBox.setText("");
		descrizioneTextBox.setText("");
		prezzoBaseTextBox.setText("");
		categoriaTexBox.setText("");
		dateBox.setValue(null, true);
	}
	
	/**
	 * Visualizzazione dei dati personali dell'utente
	 */
	private void showDati() {
		Label lbl0 = new Label("Benvenuto " + user.getNickname());
		Label lbl1 = new Label("Queste sono le tue informazioni");
		Label lbl2 = new Label("Nome: " + user.getNome());
		Label lbl3 = new Label("Cognome: "+ user.getCognome());
		Label lbl4 = new Label("Numero di telefono: " + user.getNumeroTel());
		Label lbl5 = new Label("Codice fiscale: " + user.getCodiceFiscale());
		Label lbl6 = new Label("Indirizzo: " + user.getIndirizzo());
		Label lbl7 = new Label("Email: " + user.getEmail());
		contentPanel.add(lbl0);
		contentPanel.add(lbl1);
		contentPanel.add(lbl2);
		contentPanel.add(lbl3);
		contentPanel.add(lbl4);
		contentPanel.add(lbl5);
		contentPanel.add(lbl6);
		contentPanel.add(lbl7);
		if (user.getSesso() != null) {
			Label lbl8 = new Label("Sesso: " + user.getSesso());
			contentPanel.add(lbl8);
		}
		if (user.getDataNascita() != null) {
			Label lbl9 = new Label("Data di nascita: " + user.getDataNascita());
			contentPanel.add(lbl9);
		}
		if (user.getLuogoNascita() != "") {
			Label lbl10 = new Label("Luogo di nascita: " + user.getLuogoNascita());
			contentPanel.add(lbl10);
		}
		clientImplementor.getOfferte(user);
		var = 2;
		clientImplementor.getOggetti();
		
		Button tornaProfiloBTN = new Button("Torna alla pagina profilo");
		tornaProfiloBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaProfiloBTN.setTitle(tornaProfilo_BTN);
		tornaProfiloBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(tornaProfiloBTN);
		
	}
	
	/**
	 * Setter di oggetti richiamato dopo la RPC
	 * Richiama il metodo adeguato in base al contesto
	 * @param result arraylist di oggetti
	 */
	public void setOggetti(ArrayList<Oggetto> result) {
		this.oggetti = result;
		switch (var) {
		case 1:
			var2 = 2;
			clientImplementor.getCategorie();
			break;
		case 2:
			showOggettiVenduti();
			break;
		}
		
	}
	
	/**
	 * Setter di categorie richiamato dopo la RPC
	 * Richiama il metodo adeguato in base al contesto
	 * @param categorie arraylist di categorie
	 */
	public void setCategorie(ArrayList<Categoria> categorie) {
		this.categorie = categorie;
		switch (var2) {
			case 1 :
				showInserimento();
				break;
			case 2 : 
				showOggetti();
				break;
		}
	}
	
	/**
	 * Setter di sottoCategorie richiamato dopo la RPC
	 * @param sottoCategorie arraylist di categorie
	 */
	private void setSottoCategorie(ArrayList<Categoria> sottoCategorie) {
		this.sottoCategorie = sottoCategorie;
	}
	
	/**
	 * Setter di oggettiCategorie richiamato dopo la RPC
	 * @param oggettiCategorie arraylist di oggetti
	 */
	private void setOggettiCategorie(ArrayList<Oggetto> oggettiCategorie) {
		this.oggettiCategorie = oggettiCategorie;
	}
	
	/**
	 * Setter di categoriaAttuale richiamato dopo la RPC
	 * @param categoriaAttuale nome della categoria attuale
	 */
	private void setCategoriaAttuale(String categoriaAttuale) {
		this.categoriaAttuale = categoriaAttuale;
	}
	
	/**
	 * Ordinamento e visualizzazione di tutti gli oggetti con asta in corso
	 */
	private void showOggetti() {
		
		// ordinamento degli oggetti in base alla data di scadenza
		Collections.sort(oggetti, new Comparator<Oggetto>() {
			public int compare(Oggetto o1, Oggetto o2) {
				return o1.getScadenzaAsta().compareTo(o2.getScadenzaAsta());
			}
		});
		
		Grid gridOggetti = new Grid(oggetti.size(), 2);
		for (int i = 0; i < oggetti.size(); i++) {
			if (oggetti.get(i).getScadenzaAsta().after(new Date())) {
				int k = i;
				gridOggetti.setWidget(i, 0, new Label(oggetti.get(i).getNome()));
				Button vediBTN = new Button("Vedi");
				vediBTN.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						showObjPage(k, oggetti);
					}
				});
				gridOggetti.setWidget(i, 1, vediBTN);
			}		
		}
		
		SplitLayoutPanel splitPanel = new SplitLayoutPanel(3);
		splitPanel.setWidth("1000px");
		splitPanel.setHeight("500px");
		splitPanel.getElement().getStyle().setProperty("border", "3px solid #e7e7e7");
		splitPanel.addNorth(new Label("Categoria attuale: " + categoriaAttuale), 30);
		
		// pannello che contiene tutte le categorie inserito nella parte sx dello splitPanel
		VerticalPanel categoriePanel = new VerticalPanel();
		categoriePanel.add(new Label("Categorie:"));
		for (int i = 0; i < categorie.size(); i++) {
			int k = i;
			if (categorie.get(i).getPadre().equals("")) {
				Anchor categoriaAnchor = new Anchor(categorie.get(i).getNome());
				categoriaAnchor.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						setCategoriaAttuale(categorie.get(k).getNome());
						// vengono settate le sottocategorie e oggetti 
						setSottoCategorie(getSottoCategoriePrimoLivello(categorie.get(k)));
						setOggettiCategorie(getOggettiCategoria(categorie.get(k), new ArrayList<Oggetto>()));
						// viene fatto un refresh della schermata
						contentPanel.clear();
						clientImplementor.getOggetti();
					}
					
				});
				categoriePanel.add(categoriaAnchor);
			}
		}
		
		// vengono aggiunte tutte le categorie ad ovest
		splitPanel.addWest(categoriePanel,150);
		
		Grid gridSottocategorie;
		// se sottocategorie è stato inizializzato
		if (sottoCategorie != null ) {
			gridSottocategorie = new Grid(sottoCategorie.size() + 1, 1);
			gridSottocategorie.setWidget(0, 0, new Label("Sottocategorie: "));
			ArrayList<Categoria> support = sottoCategorie;
			for (int j = 0; j < support.size(); j++) {
				int k = j;
				Anchor categoriaAnchor = new Anchor(support.get(j).getNome());
				// cliccando sulla sottocategoria si ottiene lo stesso effetto di un click sulla categoria
				categoriaAnchor.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						setCategoriaAttuale(support.get(k).getNome());
						// vengono settate le sottocategorie e viene fatto un refresh della schermata
						setSottoCategorie(getSottoCategoriePrimoLivello(support.get(k)));
						setOggettiCategorie(getOggettiCategoria(support.get(k), new ArrayList<Oggetto>()));
						contentPanel.clear();
						clientImplementor.getOggetti();
					}
					
				});
				gridSottocategorie.setWidget(j + 1, 0, categoriaAnchor);
			}
			// vengono aggiunte le categorie a nord
			splitPanel.addWest(gridSottocategorie, 100);
		} else {
			splitPanel.addWest(new Label("Questa categoria non ha nessuna sottocategoria."), 50);
		}
		
		ScrollPanel centerScrollable;
		Grid gridOggettiCat;
		
		if (oggettiCategorie != null) {
			gridOggettiCat = new Grid(oggettiCategorie.size(), 2);
			for (int i = 0; i < oggettiCategorie.size(); i++) {
				if (oggetti.get(i).getScadenzaAsta().after(new Date())) {
					// variabili di supporto per usare l'onClick
					int k = i;
					ArrayList<Oggetto> support = oggettiCategorie;
					gridOggettiCat.setWidget(i, 0, new Label(oggettiCategorie.get(i).getNome()));
					Button vediBTN = new Button("Vedi");
					vediBTN.addClickHandler(new ClickHandler() {
						
						@Override
						public void onClick(ClickEvent event) {
							showObjPage(k, support);
						}
					});
					gridOggettiCat.setWidget(i, 1, vediBTN);
				}
			}
			oggettiCategorie = null;
			centerScrollable = new ScrollPanel(gridOggettiCat);
		} else {
			centerScrollable = new ScrollPanel(new Label("Questa categoria non ha nessun oggetto."));
		}
		
		// se sono nella categoria base mostro tutti gli oggetti
		if (sottoCategorie == null) {
			centerScrollable = new ScrollPanel(gridOggetti);
			// viene riportato a null per consenitire di ricrearlo in seguito ad un click successivo
			sottoCategorie = null;
		}
		
		splitPanel.add(centerScrollable);
		contentPanel.add(splitPanel);
		
		Button tornaProfiloBTN = new Button("Torna alla pagina profilo");
		tornaProfiloBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaProfiloBTN.setTitle(tornaProfilo_BTN);
		tornaProfiloBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(tornaProfiloBTN);
		enclousurePanel.add(contentPanel);
		
	}
	
	/**
	 * Visualizzazione degli oggetti venduti dall'utente
	 */
	private void showOggettiVenduti() {
		
		contentPanel.add(new Label(" "));
		contentPanel.add(new Label("Oggetti messi in vendita"));
		ArrayList<Oggetto> oggettiVenduti = new ArrayList<Oggetto>();
		for (int i = 0; i < oggetti.size(); i++) {
			if (oggetti.get(i).getVenditore().getNickname().equals(user.getNickname())) {
				oggettiVenduti.add(oggetti.get(i));
			}
		}
		Grid gridOggetti = new Grid(oggettiVenduti.size(), 3);
		for (int i = 0; i < oggettiVenduti.size(); i++) {
			int k = i;
			gridOggetti.setWidget(i, 0, new Label(oggettiVenduti.get(i).getNome()));
			if (oggettiVenduti.get(i).getScadenzaAsta().after(new Date())) {
				gridOggetti.setWidget(i, 2, new Label("Asta aperta"));
			} else 
				gridOggetti.setWidget(i, 2, new Label("Asta chiusa"));
			Button vediBTN = new Button("Vedi");
			vediBTN.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					showObjPage(k, oggettiVenduti);
				}
			});
			gridOggetti.setWidget(i, 1, vediBTN);	
		}
		contentPanel.add(gridOggetti);
		contentPanel.add(new Label("--------------------------------------------------"));
		
		
	}
	
	/**
	 * Visualizzazione pagina dell'oggetto
	 * @param obj indice nel vettore dell'oggetto
	 * @param oggetti vettore di oggetti
	 */
	private void showObjPage(int obj, ArrayList<Oggetto> oggetti) {
		contentPanel.clear();
		contentPanel.add(new Label("Nome: " + oggetti.get(obj).getNome()));
		contentPanel.add(new Label("Descrizione: " + oggetti.get(obj).getDescrizione()));
		contentPanel.add(new Label("Prezzo: " + oggetti.get(obj).getPrezzoBase()));
		contentPanel.add(new Label("Categoria: " + oggetti.get(obj).getCategoria().getNome()));
		if (oggetti.get(obj).getUltimaOfferta() != null) {
			contentPanel.add(new Label("Ultima offerta: " + oggetti.get(obj).getUltimaOfferta().getImporto()));
			contentPanel.add(new Label("Ultimo offerente: " + oggetti.get(obj).getUltimaOfferta().getOfferente().getNome()));
		}
		contentPanel.add(new Label("Scadenza asta: " + oggetti.get(obj).getScadenzaAsta()));
		contentPanel.add(new Label("Venditore: " + oggetti.get(obj).getVenditore().getNickname()));
		Button offriBTN = new Button("Fai un'offerta");
		offriBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		offriBTN.setTitle(offri_BTN);
		offriBTN.addClickHandler(new ButtonClickHandler());
		Button domandaBTN = new Button("Fai una domanda");
		domandaBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		domandaBTN.setTitle(domanda_BTN);
		domandaBTN.addClickHandler(new ButtonClickHandler());
		Button tornaOggettiBTN = new Button("Torna all'elenco oggetti");
		tornaOggettiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaOggettiBTN.setTitle(oggetti_BTN);
		tornaOggettiBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(offriBTN);
		contentPanel.add(domandaBTN);
		contentPanel.add(tornaOggettiBTN);
		
		clientImplementor.getDomande(oggetti.get(obj));
		
	}
	
	/**
	 * Inserimento di un'offerta
	 * @param oggetto oggetto a cui si riferisce l'offerta
	 */
	private void doOfferta(Oggetto oggetto) {
		contentPanel.clear();
		contentPanel.add(new Label("Importo: "));
		TextBox importoTextBox = new TextBox();
		importoTextBox.setStyleName("form-control");
		importoTextBox.getElement().setPropertyString("placeholder", "Deve essere maggiore del prezzo attuale");
		contentPanel.add(importoTextBox);
		contentPanel.add(new Button("Inserisci", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				importo = Double.parseDouble(importoTextBox.getText().trim());
				importoTextBox.setText("");
				if (oggetto.getUltimaOfferta() != null) {
					if (importo > oggetto.getPrezzoBase() && importo > oggetto.getUltimaOfferta().getImporto()) {
						Offerta offerta = new Offerta(importo, oggetto, user, new Date().hashCode());
						oggetto.setUltimaOfferta(offerta);
						clientImplementor.aggiornaOggetto(oggetto);
						clientImplementor.nuovaOfferta(offerta);
					} else {
						updateStatusLabel("L'importo inserito è troppo basso");
					}
				} else {
					if (importo > oggetto.getPrezzoBase()) {
						Offerta offerta = new Offerta(importo, oggetto, user, new Date().hashCode());
						oggetto.setUltimaOfferta(offerta);
						clientImplementor.aggiornaOggetto(oggetto);
						clientImplementor.nuovaOfferta(offerta);
					} else {
						updateStatusLabel("L'importo inserito è troppo basso");
					}
				}
			}
		})); 

		Button tornaOggettiBTN = new Button("Torna all'elenco oggetti");
		tornaOggettiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaOggettiBTN.setTitle(oggetti_BTN);
		tornaOggettiBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(tornaOggettiBTN);
		enclousurePanel.add(contentPanel);


	}
	
	/**
	 * Inserimento di una domanda
	 * @param oggetto oggetto a cui si riferisce la domanda
	 */
	private void doDomanda(Oggetto oggetto) {
		contentPanel.clear();
	    contentPanel.add(new Label("Domanda: "));
	    TextBox domandaTextBox = new TextBox();
	    domandaTextBox.setStyleName("form-control");
	    domandaTextBox.getElement().setPropertyString("placeholder", "domanda");
	    contentPanel.add(domandaTextBox);
	    contentPanel.add(new Button("Invia", new ClickHandler(){

	      @Override
	      public void onClick(ClickEvent event) {
	        String dom = domandaTextBox.getText().trim();
	        Domanda domanda = new Domanda(dom, oggetto, new Date().hashCode());
	        clientImplementor.nuovaDomanda(domanda);
	        domandaTextBox.setText("");
	      }
	    
	    }));
	    
	    
	    
	    Button tornaOggettiBTN = new Button("Torna all'elenco oggetti");
	    tornaOggettiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
	    tornaOggettiBTN.setTitle(oggetti_BTN);
	    tornaOggettiBTN.addClickHandler(new ButtonClickHandler());
	    contentPanel.add(tornaOggettiBTN);
	    enclousurePanel.add(contentPanel);

	  }
	
	/**
	 * Setter di domande chiamato dopo la RPC
	 * @param result arraylist di domande
	 */
	public void setDomande(ArrayList<Domanda> result) {
		domande = result;
		showDomande();
	}
	
	/**
	 * Visualizzazione di tutte le domande per un oggetto
	 */
	private void showDomande() {
		contentPanel.add(new Label("Numero di domande:    " + domande.size()));
		Grid gridDomande = new Grid(domande.size(), 2);
		for (int i = 0; i < domande.size(); i++) {
			int indexDomanda = i;
			gridDomande.setWidget(i, 0, new Label(domande.get(i).getTesto()));
			clientImplementor.getRisposta(domande.get(i));
			if (domande.get(i).getOggetto().getVenditore().getNickname().equals(user.getNickname())) {
				Button rispondiButton = new Button("Rispondi");
				rispondiButton.addClickHandler(new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						rispondi(indexDomanda);
						
					}
				});
				gridDomande.setWidget(i, 1, rispondiButton);
			}	
		}
		contentPanel.add(gridDomande);
		enclousurePanel.add(contentPanel);
	}
	
	/**
	 * Inserimento della risposta ad una domanda
	 * @param indexDomanda indice della domanda nel vettore domande
	 */
	private void rispondi(int indexDomanda) {
		contentPanel.clear();
		contentPanel.add(new Label(domande.get(indexDomanda).getTesto()));
		TextArea rispostaTextArea = new TextArea();
		rispostaTextArea.setCharacterWidth(100);
		rispostaTextArea.setVisibleLines(8);
		rispostaTextArea.getElement().setPropertyString("placeholder", "Se hai già risposto a questa domanda sovrascriverai la risposta precedente.");
		Button rispondiButton = new Button("Rispondi");
		rispondiButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				String testoRisposta = rispostaTextArea.getText();
				Risposta risposta = new Risposta(testoRisposta, domande.get(indexDomanda), new Date().hashCode()); 
				clientImplementor.nuovaRisposta(risposta);
				rispostaTextArea.setText("");
				
			}
		});
		Button tornaOggettiBTN = new Button("Torna all'elenco oggetti");
		tornaOggettiBTN.setStyleName("btn btn-primary margin-horizontal margin-vertical");
		tornaOggettiBTN.setTitle(oggetti_BTN);
		tornaOggettiBTN.addClickHandler(new ButtonClickHandler());
		contentPanel.add(rispostaTextArea);
		contentPanel.add(rispondiButton);
		contentPanel.add(tornaOggettiBTN);
		enclousurePanel.add(contentPanel);
	}
	
	/**
	 * Setter di offerte chiamato dopo la RPC e visualizzazione delle stesse
	 * @param offerte arraylist di offerte
	 */
	public void setOfferte(ArrayList<Offerta> offerte) {
		contentPanel.add(new Label("--------------------------------------------------"));
		contentPanel.add(new Label("Offerte"));
		for (int i = 0; i < offerte.size(); i++) {
			// non è scaduta
			if (offerte.get(i).getOggetto().getScadenzaAsta().after(new Date())) {
				if (offerte.get(i).getOggetto().getUltimaOfferta().getOfferente().getNickname().equals(user.getNickname())) {
					contentPanel.add(new Label(offerte.get(i).getOggetto().getNome() + "      Asta in corso e migliore offerta"));
				} else 
					contentPanel.add(new Label(offerte.get(i).getOggetto().getNome() + "      Asta in corso e offerta superata"));
				
			} else {
				// è scaduta
				if (offerte.get(i).getOggetto().getUltimaOfferta().getOfferente().getNickname() == user.getNickname()) {
					contentPanel.add(new Label(offerte.get(i).getOggetto().getNome() + "      Aggiudicato"));
				} else {
					contentPanel.add(new Label(offerte.get(i).getOggetto().getNome() + "      Non aggiudicato"));
				}
			}
		}
	}
	
	/**
	 * Restituisce tutte le sottocategorie di primo livello di una categorie
	 * @param cat categoria di vui si vogliono conoscere le sottocategorie
	 * @return sottocategorie arraylist di categorie
	 */
	public ArrayList<Categoria> getSottoCategoriePrimoLivello(Categoria cat) {
		ArrayList<Categoria> sottocategorie = new ArrayList<>();
		for (int i = 0; i < categorie.size(); i++) {
			// se la categoria ha come padre 
			if (categorie.get(i).getPadre().equals(cat.getNome())) {
				sottocategorie.add(categorie.get(i));
			}
		}
		return sottocategorie;
	}
	
	/**
	 * Restituisce tutti gli oggetti di una categoria (compresi quelli delle sottocategorie)
	 * Il metodo è ricorsivo
	 * @param cat categoria di cui si vogliono gli oggetti
	 * @param oggettiCategoria arraylist di oggeti inizialmente vuoto
	 * @return result arraylist di oggetti
	 */
    public ArrayList<Oggetto> getOggettiCategoria(Categoria cat, ArrayList<Oggetto> oggettiCategoria) {
    	
    	ArrayList<Oggetto> result = new ArrayList<Oggetto>();
    	
    	for (int i = 0; i < oggetti.size(); i++) {
			// se la categoria ha come padre 
			if (oggetti.get(i).getCategoria().getNome().equals(cat.getNome())) {
				oggettiCategoria.add(oggetti.get(i));
			}
		}
    	ArrayList<Categoria> sottocategorie = getSottoCategoriePrimoLivello (cat);
    	if (sottocategorie.size() > 0) {
			for (int i = 0; i < sottocategorie.size(); i++) {
				result = getOggettiCategoria(sottocategorie.get(i), oggettiCategoria);			
			}
    	} else {
    		result = oggettiCategoria;
    	}
    	return result;
    }
	
	/**
	 * Setter di riposta chiamato dopo la RPC
	 * @param risposta risposta ricevuta
	 */
	public void setRisposta(Risposta risposta) {
		if (risposta != null)
			contentPanel.add(new Label(risposta.getTesto()));
	}
	
	
	@Override
	public void updateGui(Utente user) {
		resetLabels();
	}

	@Override
	public void updateStatusLabel(String txt) {
		Window.alert(txt);	
	}
	

}
