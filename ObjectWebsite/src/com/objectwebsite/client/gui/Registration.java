package com.objectwebsite.client.gui;

import java.util.Date;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.objectwebsite.client.model.Utente;
import com.objectwebsite.client.model.UtenteRegistrato;
import com.objectwebsite.client.service.ClientImplementor;

public class Registration extends Composite implements GuiInterface {
	
	private TextBox nicknameTextBox;
	private PasswordTextBox passwordTextBox;
	private TextBox nomeTextBox;
	private TextBox cognomeTextBox;
	private TextBox numeroTelTextBox;
	private TextBox cfTextBox;
	private TextBox indirizzoTextBox;
	private TextBox emailTextBox;
	private RadioButton rb0;
	private RadioButton rb1;
	private DateBox dateBox;
	private TextBox luogoTextBox;
	private ClientImplementor clientImplementor;
	private VerticalPanel enclousurePanel = new VerticalPanel();
	private Grid gridPanel = new Grid(12, 3);
	
	/**
	 * Costruttore e form di registrazioje
	 * @param clientImplementor oggetto clientImplementor per effettuare le RPC
	 */
	public Registration(ClientImplementor clientImplementor) {
		initWidget(this.enclousurePanel);
		this.clientImplementor = clientImplementor;
		
		Label lbl0 = new Label("Username: (*)");
		nicknameTextBox = new TextBox();
		nicknameTextBox.setStyleName("form-control");
		nicknameTextBox.getElement().setPropertyString("placeholder", "Nickanme");
		gridPanel.setWidget(0, 0, lbl0);
		gridPanel.setWidget(0, 1, nicknameTextBox);
		
		Label lbl1 = new Label("Password: (*)");
		passwordTextBox = new PasswordTextBox();
		passwordTextBox.setStyleName("form-control");
		passwordTextBox.getElement().setPropertyString("placeholder", "Password");
		gridPanel.setWidget(1, 0, lbl1);
		gridPanel.setWidget(1, 1, passwordTextBox);
		
		Label lbl2 = new Label("Nome: (*)");
		nomeTextBox = new TextBox();
		nomeTextBox.setStyleName("form-control");
		nomeTextBox.getElement().setPropertyString("placeholder", "Nome");
		gridPanel.setWidget(3, 0, lbl2);
		gridPanel.setWidget(3, 1, nomeTextBox);
		
		Label lbl3 = new Label("Cognome: (*)");
		cognomeTextBox = new TextBox();
		cognomeTextBox.setStyleName("form-control");
		cognomeTextBox.getElement().setPropertyString("placeholder", "Cognome");
		gridPanel.setWidget(4, 0, lbl3);
		gridPanel.setWidget(4, 1, cognomeTextBox);
		
		Label lbl4 = new Label("Numero di telefono: (*)");
		numeroTelTextBox = new TextBox();
		numeroTelTextBox.setStyleName("form-control");
		numeroTelTextBox.getElement().setPropertyString("placeholder", "Telefono");
		gridPanel.setWidget(5, 0, lbl4);
		gridPanel.setWidget(5, 1, numeroTelTextBox);
		
		Label lbl5 = new Label("Codice fiscale: (*)");
		cfTextBox = new TextBox();
		cfTextBox.setStyleName("form-control");
		cfTextBox.getElement().setPropertyString("placeholder", "Codice fiscale");
		gridPanel.setWidget(6, 0, lbl5);
		gridPanel.setWidget(6, 1, cfTextBox);
		
		Label lbl6 = new Label("Indirizzo: (*)");
		indirizzoTextBox = new TextBox();
		indirizzoTextBox.setStyleName("form-control");
		indirizzoTextBox.getElement().setPropertyString("placeholder", "Indirizzo");
		gridPanel.setWidget(7, 0, lbl6);
		gridPanel.setWidget(7, 1, indirizzoTextBox);
		
		Label lbl7 = new Label("Email: (*)");
		emailTextBox = new TextBox();
		emailTextBox.setStyleName("form-control");
		emailTextBox.getElement().setPropertyString("placeholder", "Email");
		gridPanel.setWidget(8, 0, lbl7);
		gridPanel.setWidget(8, 1, emailTextBox);
		
		Label lbl8 = new Label("Sesso: ");
		rb0 = new RadioButton("Sesso", "Maschio");
		rb1 = new RadioButton("Sesso", "Femmina");
		gridPanel.setWidget(9, 0, lbl8);
		gridPanel.setWidget(9, 1, rb0);
		gridPanel.setWidget(9, 2, rb1);
		
		Label lbl9 = new Label("Data di nascita: ");
		DateTimeFormat dateFormat = DateTimeFormat.getLongDateFormat();
		dateBox = new DateBox();
		dateBox.setFormat(new DateBox.DefaultFormat(dateFormat));
		dateBox.getDatePicker().setYearArrowsVisible(true);
		dateBox.addValueChangeHandler(new ValueChangeHandler<Date>() {		
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();
				String dateString = DateTimeFormat.getMediumDateFormat().format(date);
			}
		});
		dateBox.setValue(null, true);
		gridPanel.setWidget(10, 0, lbl9);
		gridPanel.setWidget(10, 1, dateBox);
		
		Label lbl10 = new Label("Luogo di nascita: ");
		luogoTextBox = new TextBox();
		luogoTextBox.setStyleName("form-control");
		luogoTextBox.getElement().setPropertyString("placeholder", "Luogo di nascita");
		gridPanel.setWidget(11, 0, lbl10);
		gridPanel.setWidget(11, 1, luogoTextBox);
		
		enclousurePanel.add(gridPanel);
		enclousurePanel.add(new Label("(*) Questi campi sono obbligatori."));
		
		// creazione pulsante registrazione
		Button registratiBTN = new Button("Registrati", new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				String username = nicknameTextBox.getText().trim();
				String password = passwordTextBox.getText().trim();
				String nome = nomeTextBox.getText().trim();
				String cognome = cognomeTextBox.getText().trim();
				int numeroTel = Integer.parseInt(numeroTelTextBox.getText().trim());
				String cf = cfTextBox.getText().trim();
				String indirizzo = indirizzoTextBox.getText().trim();
				String email = emailTextBox.getText().trim();
				String sesso = "";
			
		        if(rb0.getValue()) {
		          sesso="Maschio";
		        } else if(rb1.getValue()) {
		          sesso="Femmina";
		        }
				Date dataNascita = (Date) dateBox.getValue();
					
				String luogoNascita = luogoTextBox.getText().trim();
				UtenteRegistrato utente = new UtenteRegistrato(username, password, nome, cognome, numeroTel,
						cf, indirizzo, email);
				if (sesso != "")
					utente.setSesso(sesso);
				if (dataNascita != null)
					utente.setDataNascita(dataNascita);
				if (luogoNascita != null)
					utente.setLuogoNascita(luogoNascita);
				
				if ((username.equals("admin") && password.equals("admin")) || (dataNascita!= null && dataNascita.after(new Date()))) {
					Window.alert("Le credenziali non sono valide");
					resetLabels();
				} else {
					// invio con RPC
					clientImplementor.registration(utente);
					resetLabels();
				}
			}
		});	

		Button vaiLoginBTN = new Button("Vai al login", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				clientImplementor.getMainGUI().doLogin();
			}
		});
		
		enclousurePanel.add(registratiBTN);
		enclousurePanel.add(vaiLoginBTN);
		
	
	}
	
	/**
	 * Ripristina i valori di default delle etichette
	 */
	private void resetLabels() {
		nicknameTextBox.setText("");
		passwordTextBox.setText("");
		nomeTextBox.setText("");
		cognomeTextBox.setText("");
		numeroTelTextBox.setText("");
		cfTextBox.setText("");
		indirizzoTextBox.setText("");
		emailTextBox.setText("");
		rb0.setValue(false);
		rb1.setValue(false);
		dateBox.setValue(null, true);
		luogoTextBox.setText("");
		
	}
	
	@Override
	public void updateGui(Utente sessionUser) {
		resetLabels();
		
	}

	@Override
	public void updateStatusLabel(String txt) {
		Window.alert(txt);
		
	}

}
	
