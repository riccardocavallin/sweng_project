package com.objectwebsite.client.model;

import java.io.Serializable;
import java.util.ArrayList;



public class Categoria implements Serializable {
	
	private String nome;
	private String padre;
	
	public Categoria() {		
	}
	
	public Categoria(String nome, String padre) {
		this.nome = nome;
		this.padre = padre;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getPadre() {
		return padre;
	}
	
	// metodo usato per far si che questa diventi la sottocategoria di un'altra categoria
	public void setPadre(String padre) {
		this.padre = padre;
	}
	
}
