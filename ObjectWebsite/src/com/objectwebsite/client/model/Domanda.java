package com.objectwebsite.client.model;

import java.io.Serializable;

import java_cup.internal_error;

public class Domanda implements Serializable {
	
	private String testo;
	private Oggetto oggetto;
	private int id;
	
	public Domanda() {	
	}
	
	public Domanda(String testo, Oggetto oggetto, int id) {
		this.testo = testo;
		this.oggetto = oggetto;
		this.id = id;
	}
	
	public String getTesto() {
		return testo;
	}
	
	public void setTesto(String testo) {
		this.testo = testo;
	}
	
	public Oggetto getOggetto() {
		return oggetto;
	}
	
	public void setOggetto(Oggetto oggetto) {
		this.oggetto = oggetto;
	}

	public int getId() {
		return id;
	}
	
}
