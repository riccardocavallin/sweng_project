package com.objectwebsite.client.model;

import java.io.Serializable;

public class Offerta implements Serializable{
	
	private Double importo;
	private Oggetto oggetto;
	private UtenteRegistrato offerente;
	private static int count = 0;
	private int id;
	
	public Offerta() {
	}
	
	public Offerta(Double importo, Oggetto oggetto, UtenteRegistrato offerente, int id) {
		this.importo = importo;
		this.oggetto = oggetto;
		this.offerente = offerente;
		this.id = id;
	}
	
	public Double getImporto() {
		return importo;
	}
	
	public Oggetto getOggetto() {
		return oggetto;
	}
	
	public UtenteRegistrato getOfferente() {
		return offerente;
	}
	
	public int getId() {
		return id;
	}
}
