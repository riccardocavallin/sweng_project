package com.objectwebsite.client.model;

import java.io.Serializable;
import java.util.Date;

public class Oggetto implements Serializable {
	
	private String nome;
	private String descrizione;
	private Double prezzoBase;
	private Categoria categoria;
	private Date scadenzaAsta;
	private UtenteRegistrato venditore;
	private Offerta ultimaOfferta;
	private int id;
	
	public Oggetto() {	
	}
	
	public Oggetto(String nome, String descrizione, Double prezzoBase, Categoria categoria, Date scadenzaAsta, UtenteRegistrato venditore, int id) {
		this.nome = nome;
		this.descrizione = descrizione;
		this.prezzoBase = prezzoBase;
		this.categoria = categoria;
		this.scadenzaAsta = scadenzaAsta;
		this.venditore = venditore;
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public Double getPrezzoBase() {
		return prezzoBase;
	}
	
	public void setPrezzoBase(Double prezzoBase) {
		this.prezzoBase = prezzoBase;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
	public Date getScadenzaAsta() {
		return scadenzaAsta;
	}
	
	public UtenteRegistrato getVenditore() {
		return venditore;
	}
	

	public Offerta getUltimaOfferta() {
		return ultimaOfferta;
	}

	public void setUltimaOfferta(Offerta ultimaOfferta) {
		this.ultimaOfferta = ultimaOfferta;
	}

	public int getId() {
		return id;
	}
	
}
