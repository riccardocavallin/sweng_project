package com.objectwebsite.client.model;

import java.io.Serializable;

import java_cup.internal_error;

public class Risposta implements Serializable {
	
	private String testo;
	private Domanda domanda;
	private int id;
	
	public Risposta() {	
	}
	
	public Risposta(String testo, Domanda domanda, int id) {
		this.testo = testo;
		this.domanda = domanda;
		this.id = id;
	}
	
	public String getTesto() {
		return testo;
	}
	
	public void setTesto(String testo) {
		this.testo = testo;
	}
	
	public Domanda getDomanda() {
		return domanda;
	}
	
	public void setDomanda(Domanda domanda) {
		this.domanda = domanda;
	}
	
	public int getId() {
		return id;
	}
}
