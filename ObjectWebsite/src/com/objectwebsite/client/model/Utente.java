package com.objectwebsite.client.model;

import java.io.Serializable;

public class Utente implements Serializable{
	
	public String nickname;
	public String password;
	
	public Utente() {
	}
	
	public Utente(String username, String password) {
		this.nickname = username;
		this.password = password;
	}
	
	public String getPassword() {
		return password;		
	}
	
	public String getNickname() {
		return nickname;
	}
}
