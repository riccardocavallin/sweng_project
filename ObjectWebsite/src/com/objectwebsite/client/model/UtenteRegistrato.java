package com.objectwebsite.client.model;

import java.io.Serializable;
import java.util.Date;

public class UtenteRegistrato extends Utente implements Serializable {
	
	private String nome;
	private String cognome;
	private int numeroTel;
	private String codiceFiscale;
	private String indirizzo;
	private String email;
	private String sesso;
	private Date dataNascita;
	private String luogoNascita;
	
	public UtenteRegistrato() {
	}
		
	public UtenteRegistrato(String nickname, String password, String nome, String cognome, int numeroTel,
			String codiceFiscale, String indirizzo, String email) {
		
		super(nickname,password);
		this.nome = nome;
		this.cognome = cognome;
		this.numeroTel = numeroTel;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getNumeroTel() {
		return numeroTel;
	}

	public void setNumeroTel(int numeroTel) {
		this.numeroTel = numeroTel;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public void aggiungiDomanda(Domanda domanda) {
		//TODO
	}
	
	public void aggiungiRisposta(String testo, Risposta risposta, Oggetto oggetto) {
		//TODO
	}
	
}
