package com.objectwebsite.client.service;

import java.util.ArrayList;

import org.junit.experimental.categories.Categories;

import com.google.gwt.core.client.GWT;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.objectwebsite.client.service.ServiceAsync;
import com.objectwebsite.client.gui.Main;
import com.objectwebsite.client.model.*;
import com.objectwebsite.client.gui.*;;

public class ClientImplementor implements ClientImplementorInterface {

	private ServiceAsync serviceAsync;
	private Main main;
	private ProfiloUtente profiloUtente;

	public ClientImplementor(String url) {
		// creazione istanza per rpc calls al server
		this.serviceAsync = GWT.create(Service.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.serviceAsync;
		endpoint.setServiceEntryPoint(url); // URL is the servlet url

		this.main = new Main(this);
	}

	public Main getMainGUI() {
		return this.main;
	}

	@Override
	public void login(String nickname, String password) {
		this.serviceAsync.login(nickname, password, new LoginCallback());

	}

	@Override
	public void getUser(String nickname) {
		this.serviceAsync.getUser(nickname, new UserCallback());

	}


	@Override
	public void registration(UtenteRegistrato utente) {
		this.serviceAsync.registration(utente, new RegisteredCallback());

	}

	@Override
	public void nuovoOggetto(Oggetto oggetto) {
		this.serviceAsync.nuovoOggetto(oggetto, new NuovoOggettoCallback());

	}

	@Override
	public void getOggetti() {
		this.serviceAsync.getOggetti(new OggettiCallBack());
	}

	@Override
	public void aggiornaOggetto(Oggetto oggetto) {
		this.serviceAsync.nuovoOggetto(oggetto, new AggiornaOggettoCallback());

	}

	@Override
	public void nuovaDomanda(Domanda domanda) {
		this.serviceAsync.nuovaDomanda(domanda, new NuovaDomandaCallBack());

	}

	@Override
	public void getDomande(Oggetto oggetto) {
		this.serviceAsync.getDomande(oggetto, new DomandeCallBack());

	}

	@Override
	public void nuovaRisposta(Risposta risposta) {
		this.serviceAsync.nuovaRisposta(risposta, new NuovaRispostaCallBack());
	}

	@Override
	public void nuovaOfferta(Offerta offerta) {
		this.serviceAsync.nuovaOfferta(offerta, new NuovaOffertaCallBack());
	}

	@Override
	public void getOfferte(UtenteRegistrato utente) {
		this.serviceAsync.getOfferte(utente, new GetOfferteCallBack());

	}

	@Override
	public void getRisposta(Domanda domanda) {
		this.serviceAsync.getRisposta(domanda, new GetRispostaCallBack());
	}
	
	@Override
	public void nuovaCategoria(Categoria categoria) {
		this.serviceAsync.nuovaCategoria(categoria, new NuovaCategoriaCallback());
	}
	
	@Override
	public void getCategoria(String nome) {
		this.serviceAsync.getCategoria(nome, new GetCategoriaCallback());
	}
	
	@Override
	public void getCategorie() {
		this.serviceAsync.getCategorie(new GetCategorieCallBack());
	}
	
	@Override
	public void eliminaDomanda(Domanda domanda) {
		this.serviceAsync.eliminaDomanda(domanda, new EliminaDomandaCallBack());
		
	}

	@Override
	public void eliminaRisposta(Risposta risposta) {
		this.serviceAsync.eliminaRisposta(risposta, new EliminaRispostaCallBack());
	}
	
	@Override
	public void getRisposte() {
		this.serviceAsync.getRisposte( new GetRisposteCallBack());
	}
	
	@Override
	public void aggiornaCategoria(String nomeVecchio, String nuovoNome) {
		this.serviceAsync.aggiornaCategoria(nomeVecchio, nuovoNome,  new AggiornaCategoriaCallBack());
	}
	
	@Override
	public void eliminaOfferta(Offerta offerta) {
		this.serviceAsync.eliminaOfferta(offerta, new EliminaOffertaCallBack());
	}
	
	@Override
	public void eliminaOggetto(Oggetto oggetto) {
		this.serviceAsync.eliminaOggetto(oggetto, new EliminaOggettoCallBack());
	}
	
	@Override
	public void getOffertePerOggetto(Oggetto oggetto) {
		this.serviceAsync.getOffertePerOggetto(oggetto, new GetOffertePerOggettoCallBack());
	}


	private class LoginCallback implements AsyncCallback<UtenteRegistrato> {
		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nel login");
		}

		@Override
		public void onSuccess(UtenteRegistrato result) {
			// se restituisce un utente
			if(result != null) {
				main.loginUser(result);
			} else {
				main.updateStatusLabel("Errore nel login");
			}

		}
	}

	private class UserCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("An error has occured");
		}

		@Override
		public void onSuccess(Object result) {
			System.out.println("Response received");
			
		}
	}

	private class RegisteredCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella registrazione");
		}

		@Override
		public void onSuccess(Object result) {
			main.updateStatusLabel("Registrazione effettuata con successo");

		}
	}

	private class NuovoOggettoCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nell'inserimento");
		}

		@Override
		public void onSuccess(Object result) {
			main.updateStatusLabel("Inserimento effettuato con successo");

		}
	}

	private class OggettiCallBack implements AsyncCallback<ArrayList<Oggetto>> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione degli oggetti");

		}

		@Override
		public void onSuccess(ArrayList<Oggetto> result) {
			main.setOggetti(result);

		} 

	}

	private class AggiornaOggettoCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			main.userProfile.updateStatusLabel("Errore nell'aggiornamento");
		}

		@Override
		public void onSuccess(Object result) {
			//main.userProfile.updateStatusLabel("Aggiornamento oggetto effettuato");

		}
	}

	private class NuovaDomandaCallBack implements AsyncCallback {

		@Override
		public void onFailure(Throwable caught) {
			main.userProfile.updateStatusLabel("Errore nell'inserimento della domanda");

		}

		@Override
		public void onSuccess(Object result) {
			main.userProfile.updateStatusLabel("Domanda inserita");

		}

	}

	private class DomandeCallBack implements AsyncCallback<ArrayList<Domanda>> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione degli oggetti");

		}

		@Override
		public void onSuccess(ArrayList<Domanda> result) {
			main.setDomande(result);

		} 

	}

	private class NuovaRispostaCallBack implements AsyncCallback {

		@Override
		public void onFailure(Throwable caught) {
			main.userProfile.updateStatusLabel("Errore nell'inserimento della risposta");

		}

		@Override
		public void onSuccess(Object result) {
			main.userProfile.updateStatusLabel("Risposta inserita");

		}

	}

	private class GetOfferteCallBack implements AsyncCallback<ArrayList<Offerta>> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione delle offerte");

		}


		@Override
		public void onSuccess(ArrayList<Offerta> result) {
			main.userProfile.setOfferte(result);

		} 

	}

	private class NuovaOffertaCallBack implements AsyncCallback {

		@Override
		public void onFailure(Throwable caught) {
			main.userProfile.updateStatusLabel("Errore nell'inserimento dell'offerta");

		}

		@Override
		public void onSuccess(Object result) {
			main.userProfile.updateStatusLabel("Offerta inserita");

		}

	}
	
	private class GetRispostaCallBack implements AsyncCallback<Risposta> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione deglla risposta");

		}

		@Override
		public void onSuccess(Risposta result) {
			main.userProfile.setRisposta(result);

		} 

	}
	
	private class NuovaCategoriaCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			main.admin.updateStatusLabel("Errore nella creazione della categoria");
		}

		@Override
		public void onSuccess(Object result) {
			main.admin.updateStatusLabel("Nuova categoria creata");

		}
	}
	
	private class GetCategoriaCallback implements AsyncCallback<Categoria> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione deglla risposta");

		}

		@Override
		public void onSuccess(Categoria result) {
			main.admin.setCategoria(result);

		} 

	}
	
	private class GetCategorieCallBack implements AsyncCallback<ArrayList<Categoria>> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione delle categorie");

		}

		@Override
		public void onSuccess(ArrayList<Categoria> result) {
			main.setCategorie(result);

		} 

	}
	
	private class EliminaDomandaCallBack implements AsyncCallback {

		@Override
		public void onFailure(Throwable caught) {
			main.admin.updateStatusLabel("Errore nell'eliminazione della domanda.");

		}

		@Override
		public void onSuccess(Object result) {
			main.admin.updateStatusLabel("Domanda eliminata");

		} 

	}
	
	private class EliminaRispostaCallBack implements AsyncCallback {

		@Override
		public void onFailure(Throwable caught) {
			main.admin.updateStatusLabel("Errore nell'eliminazione della risposta");

		}

		@Override
		public void onSuccess(Object result) {
			main.admin.updateStatusLabel("Risposta eliminata");

		}

	}

	private class GetRisposteCallBack implements AsyncCallback<ArrayList<Risposta>> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione delle risposte");

		}

		public void onSuccess(ArrayList<Risposta> result) {
			main.setRisposte(result);

		} 

	}
	
	private class AggiornaCategoriaCallBack implements AsyncCallback<Void> {

		@Override
		public void onFailure(Throwable caught) {
			main.admin.updateStatusLabel("Errore nell'aggiornamento della categoria");

		}

		@Override
		public void onSuccess(Void result) {
			main.admin.updateStatusLabel("Nome aggiornato");		
		}

	}
	
	private class EliminaOffertaCallBack implements AsyncCallback<Void> {

		@Override
		public void onFailure(Throwable caught) {
			main.admin.updateStatusLabel("Errore nell'eliminazione dell'offerta");

		}

		@Override
		public void onSuccess(Void result) {
			main.admin.updateStatusLabel("Offerta eliminata");		
		}

	}
	
	private class EliminaOggettoCallBack implements AsyncCallback<Void> {

		@Override
		public void onFailure(Throwable caught) {
			main.admin.updateStatusLabel("Errore nell'eliminazione dell'oggetto");

		}

		@Override
		public void onSuccess(Void result) {
			main.admin.updateStatusLabel("Oggetto eliminato");		
		}

	}
	
	private class GetOffertePerOggettoCallBack implements AsyncCallback<ArrayList<Offerta>> {

		@Override
		public void onFailure(Throwable caught) {
			main.updateStatusLabel("Errore nella restituzione delle offerte");

		}


		@Override
		public void onSuccess(ArrayList<Offerta> result) {
			main.admin.setOffertePerOggetto(result);

		} 

	}

	
}

