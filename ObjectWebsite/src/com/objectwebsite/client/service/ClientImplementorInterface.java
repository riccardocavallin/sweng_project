package com.objectwebsite.client.service;

import java.util.ArrayList;

import com.objectwebsite.client.model.*;

public interface ClientImplementorInterface {
	
	void login(String nickname, String password);

	void getUser(String nickname);

	void registration(UtenteRegistrato utente);

	void nuovoOggetto(Oggetto oggetto);

	void getOggetti();

	void aggiornaOggetto(Oggetto oggetto);

	void nuovaDomanda(Domanda domanda);
	
	void getDomande(Oggetto oggetto);
	
	void nuovaRisposta(Risposta risposta);

	void getOfferte(UtenteRegistrato utente);

	void nuovaOfferta(Offerta offerta);

	void getRisposta(Domanda domanda);

	void nuovaCategoria(Categoria categoria);

	void getCategoria(String nome);

	void getCategorie();
	
	void eliminaDomanda(Domanda domanda);
	
	void eliminaRisposta(Risposta risposta);
	
	void getRisposte();

	void aggiornaCategoria(String nomeVecchio, String nuovoNome);

	void eliminaOfferta(Offerta offerta);

	void eliminaOggetto(Oggetto oggetto);

	void getOffertePerOggetto(Oggetto oggetto);

}
