package com.objectwebsite.client.service;

import java.util.ArrayList;

import javax.servlet.Registration;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.objectwebsite.client.model.*;

@RemoteServiceRelativePath("service")
public interface Service extends RemoteService{

	UtenteRegistrato login(String nickname, String password);
	
	UtenteRegistrato getUser(String nickname);
	
	boolean registration(UtenteRegistrato utente);

	void nuovoOggetto(Oggetto oggetto);
	
	ArrayList<Oggetto> getOggetti();
	
	void aggiornaOggetto(Oggetto oggetto);
	
	void nuovaDomanda(Domanda domanda);

	ArrayList<Domanda> getDomande(Oggetto oggetto);
	
	void nuovaRisposta(Risposta risposta);
	
	void nuovaOfferta(Offerta offerta);
	
	ArrayList<Offerta> getOfferte(UtenteRegistrato utente);
	
	Risposta getRisposta(Domanda domanda);

	void nuovaCategoria(Categoria categoria);
	
	Categoria getCategoria(String nome);
	
	ArrayList<Categoria> getCategorie();

	void eliminaDomanda(Domanda domanda);

	void eliminaRisposta(Risposta risposta);

	ArrayList<Risposta> getRisposte();
	
	void aggiornaCategoria(String nomeVecchio, String nuovoNome);
	
	void eliminaOfferta(Offerta offerta);
	
	void eliminaOggetto(Oggetto oggetto); 
	
	ArrayList<Offerta> getOffertePerOggetto(Oggetto oggetto);
	
}
