package com.objectwebsite.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.objectwebsite.client.model.Categoria;
import com.objectwebsite.client.model.Domanda;
import com.objectwebsite.client.model.Offerta;
import com.objectwebsite.client.model.Oggetto;
import com.objectwebsite.client.model.Risposta;
import com.objectwebsite.client.model.UtenteRegistrato;

public interface ServiceAsync {

	void login(String nickname, String password, AsyncCallback<UtenteRegistrato> callback);

	void getUser(String nickname, AsyncCallback<UtenteRegistrato> callback);

	void registration(UtenteRegistrato utente, AsyncCallback<Boolean> callback);
	
	void nuovoOggetto(Oggetto oggetto, AsyncCallback<Void> callback);

	void getOggetti(AsyncCallback<ArrayList<Oggetto>> callback);

	void aggiornaOggetto(Oggetto oggetto, AsyncCallback<Void> callback);

	void nuovaDomanda(Domanda domanda, AsyncCallback<Void> callback);
	
	void getDomande(Oggetto oggetto, AsyncCallback<ArrayList<Domanda>> callback);

	void nuovaRisposta(Risposta risposta, AsyncCallback<Void> callback);

	void getOfferte(UtenteRegistrato utente, AsyncCallback<ArrayList<Offerta>> callback);

	void nuovaOfferta(Offerta offerta, AsyncCallback<Void> callback);

	void getRisposta(Domanda domanda, AsyncCallback<Risposta> callback);
	
	void nuovaCategoria(Categoria categoria, AsyncCallback<Void> callback);

	void getCategoria(String nome, AsyncCallback<Categoria> callback);

	void getCategorie(AsyncCallback<ArrayList<Categoria>> callback);

	void eliminaDomanda(Domanda domanda, AsyncCallback<Void> callback);

	void eliminaRisposta(Risposta risposta, AsyncCallback<Void> callback);
	
	void getRisposte(AsyncCallback<ArrayList<Risposta>> callback);

	void aggiornaCategoria(String nomeVecchio, String nuovoNome, AsyncCallback<Void> callback);

	void eliminaOfferta(Offerta offerta, AsyncCallback<Void> callback);

	void eliminaOggetto(Oggetto oggetto, AsyncCallback<Void> callback);

	void getOffertePerOggetto(Oggetto oggetto, AsyncCallback<ArrayList<Offerta>> callback);

	
}
