package com.objectwebsite.server;

import java.io.File;
import javax.servlet.ServletContext;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import com.objectwebsite.client.model.*;
import com.objectwebsite.client.service.*;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.util.*;

public class ServiceImplementor extends RemoteServiceServlet implements Service {
	
	public static final File DB_FILE = new File("database.db");
	public static final String UTENTI_MAP = "utenti";
	public static final String OGGETTI_MAP = "oggetti";
	public static final String DOMANDE_MAP = "domande";
	public static final String RISPOSTE_MAP = "risposte";
	public static final String OFFERTE_MAP = "offerte";
	public static final String CATEGORIE_MAP = "categorie";
	
	/**
	 * Restituisce il database
	 * @return 		istanza del database
	 */
	private DB getDB() {
		ServletContext context = this.getServletContext();
		// creo se necessario il db e lo restituisco
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.fileDB(DB_FILE).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);				
			}
			return db;
		}
	}
	
	/**
	 * Resgistrazione di un utente
	 * @param utente utente da registrare nel db
	 * @return 		true se ha successo, false altrimenti
	 */
	@Override
	public boolean registration(UtenteRegistrato utente) {
		DB db = getDB();
		if (!existUser(utente.getNickname())) {
			HTreeMap<Long, UtenteRegistrato> mapUsers = db.hashMap(UTENTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
			mapUsers.put((long)utente.getNickname().hashCode(), utente);
			db.commit();
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Restituisce l'utente spefificato
	 * @param nickname nickname dell'utente da restituire
	 * @return 		oggetto di tipo UtenteRegistrato se esiste, altrimenti oggetto null
	 */
	@Override
	public UtenteRegistrato getUser(String nickname) {
		DB db = getDB();
		HTreeMap<Long, UtenteRegistrato> mapUsers = db.hashMap(UTENTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		long hashNickname = (long) nickname.hashCode();
		if(mapUsers.containsKey(hashNickname))
			return mapUsers.get(hashNickname);
		else
			return null;
	}
	
	/**
	 * Effettua il login
	 * @param nickname nickname dell'utente da loggare
	 * @param password la password dell'utente
	 * @return 		oggetto di tipo UtenteRegistrato altrimenti null in caso di fallimento
	 */
	@Override
	public UtenteRegistrato login(String nickname, String password) {
		UtenteRegistrato user = getUser(nickname);
		if(user != null) {
			if(user.getPassword().equals(password))
				return user;
			else
				return null;
		} else
			return null;
	}	
	
	/**
	 * Controllo sull'esistenza di un utente nel database
	 * @param nickname nickname dell'utente
	 * @return 		true se esiste, false altrimenti
	 */
	private boolean existUser(String nickname) {
		if(getUser(nickname) == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Inserimento di un nuovo oggetto nel db
	 * @param oggetto l'oggetto da inserire 
	 */
	@Override
	public void nuovoOggetto(Oggetto oggetto) {
		DB db = getDB();
		HTreeMap<Long, Oggetto> mapOggetti = db.hashMap(OGGETTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapOggetti.put((long)oggetto.getId(), oggetto);
		db.commit();
	}
	
	/**
	 * Restituzine di tutti gli oggetti del db
	 * @return 		arraylist di oggetti
	 */
	@Override
	public ArrayList<Oggetto> getOggetti() {
		ArrayList<Oggetto> oggetti = new ArrayList<Oggetto>();
		DB db = getDB();
		Map<Long, Oggetto> mapOggetti = db.hashMap(OGGETTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		Date today = new Date(); 
		for(Map.Entry<Long, Oggetto> entry : mapOggetti.entrySet()) {
			oggetti.add(entry.getValue());
		}
		return oggetti;
	}
	
	/**
	 * Aggiornamento dell'oggetto nel db
	 * @param oggetto oggetto da aggiornare
	 */
	@Override
	public void aggiornaOggetto(Oggetto oggetto) {
		DB db = getDB();
		HTreeMap<Long, Oggetto> mapOggetti = db.hashMap(OGGETTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapOggetti.remove((long)oggetto.getId());
		mapOggetti.put((long)oggetto.getId(), oggetto);
		db.commit();
	}
	
	/**
	 * Inserimento di una domanda
	 * @param domanda domanda da inserire
	 */
	@Override
	public void nuovaDomanda(Domanda domanda) {
		DB db = getDB();
		HTreeMap<Long, Domanda> mapDomande = db.hashMap(DOMANDE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapDomande.put((long)domanda.getId(), domanda);
		db.commit();
	}
	
	/**
	 * Inserimento di una risposta
	 * @param risposta risposta da inserire
	 */
	@Override
	public void nuovaRisposta(Risposta risposta) {
		DB db = getDB();
		HTreeMap<Long, Risposta> mapRisposte = db.hashMap(RISPOSTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapRisposte.put((long)risposta.getDomanda().getId(), risposta);
		db.commit();
	}
	
	/**
	 * Restituzine di tutte le domande per un oggetto
	 * @param oggetto oggetto a cui fanno riferimento le domande
	 * @return 		arraylist di domande
	 */
	@Override
	public ArrayList<Domanda> getDomande(Oggetto oggetto) {
		ArrayList<Domanda> domande = new ArrayList<Domanda>();
		DB db = getDB();
		Map<Long, Domanda> mapDomande = db.hashMap(DOMANDE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Domanda> entry : mapDomande.entrySet()) {
			
			if (oggetto.getId() == entry.getValue().getOggetto().getId()) {
				domande.add(entry.getValue());
			}		
		}
		return domande;
	}
	
	/**
	 * Inserimento di una offerta
	 * @param offerta offerta da inserire
	 */
	@Override
	public void nuovaOfferta(Offerta offerta) {
	    DB db = getDB();
	    HTreeMap<Long, Offerta> mapOfferte = db.hashMap(OFFERTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
	    mapOfferte.put((long)offerta.getId(), offerta);
	    db.commit();
	  }
	 
	/**
	 * Restituzine di tutte le offerte di un utente
	 * @param utente utente che ha effettuato le offerte
	 * @return 		arraylist di offerte
	 */
	@Override
	public ArrayList<Offerta> getOfferte(UtenteRegistrato utente) {
		ArrayList<Offerta> offerte = new ArrayList<Offerta>();
		DB db = getDB();
		Map<Long, Offerta> mapOfferte = db.hashMap(OFFERTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Offerta> entry : mapOfferte.entrySet()) {
			if (utente.getNickname().equals(entry.getValue().getOfferente().getNickname())) {
				offerte.add(entry.getValue());
			}    
		}
		return offerte;
	}
	
	/**
	 * Restituzine di tutte le offerte per un oggetto
	 * @param oggetto oggetto a cui fanno riferimento le offerte
	 * @return 		arraylist di offerte
	 */
	@Override
	public ArrayList<Offerta> getOffertePerOggetto(Oggetto oggetto) {
		ArrayList<Offerta> offerte = new ArrayList<Offerta>();
		DB db = getDB();
		Map<Long, Offerta> mapOfferte = db.hashMap(OFFERTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Offerta> entry : mapOfferte.entrySet()) {
			if (entry.getValue().getOggetto().getId() == oggetto.getId()) {
				offerte.add(entry.getValue());
			}    
		}
		return offerte;
	}
	
	/**
	 * Restituzine della risposta ad una domanda
	 * @param domanda domanda a cui fa riferimento la domanda
	 * @return 		oggetto di tipo risposta
	 */
	@Override
	public Risposta getRisposta(Domanda domanda) {
		DB db = getDB();
		Risposta risposta = null;
		Map<Long, Risposta> mapRisposte = db.hashMap(RISPOSTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Risposta> entry : mapRisposte.entrySet()) {
			if (domanda.getId() == (entry.getValue().getDomanda().getId())) {
				risposta = entry.getValue();
			}		
		}
		if (risposta != null) {
			return risposta;
		} else return null;
		
	}
	
	/**
	 * Restituzine di tutte le risposte nel db
	 * @return 		arraylist di risposte
	 */
	@Override
	public ArrayList<Risposta> getRisposte() {
		DB db = getDB();
		ArrayList<Risposta> risposte = new ArrayList<Risposta>();
		Map<Long, Risposta> mapRisposte = db.hashMap(RISPOSTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Risposta> entry : mapRisposte.entrySet()) {
				risposte.add(entry.getValue());
		}
			return risposte;
	}
	
	/**
	 * Inserimento di una nuova categoria
	 * @param categoria categoria da inserire
	 */
	@Override
	public void nuovaCategoria(Categoria categoria) {
	    DB db = getDB();
	    HTreeMap<Long, Categoria> mapCategorie = db.hashMap(CATEGORIE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
	    mapCategorie.put((long)categoria.getNome().hashCode(), categoria);
	    db.commit();
	}
	
	/**
	 * Restituzine di una categoria
	 * @param nome nome della categoria
	 * @return 		oggetto di tipo categoria
	 */
	@Override
	public Categoria getCategoria(String nome) {
	    DB db = getDB();
	    Map<Long, Categoria> mapCategorie = db.hashMap(CATEGORIE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
	    return mapCategorie.getOrDefault((long)nome.hashCode(), null);
	}
	
	/**
	 * Restituzine di tutti le categorie nel db
	 * @return 		arraylist di categorie
	 */
	@Override
	public ArrayList<Categoria> getCategorie() {
		ArrayList<Categoria> categorie = new ArrayList<Categoria>();
		DB db = getDB();
		Map<Long, Categoria> mapCategorie = db.hashMap(CATEGORIE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Categoria> entry : mapCategorie.entrySet()) {
			categorie.add(entry.getValue());   
		}
		return categorie;
	}
	
	/**
	 * Eliminazione di una domanda dal db
	 * @param domanda domanda da eliminare
	 */
	@Override
	public void eliminaDomanda (Domanda domanda) {
		DB db = getDB();
		Map<Long, Domanda> mapDomande = db.hashMap(DOMANDE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		Risposta risposta = getRisposta(domanda);
		eliminaRisposta(risposta);
		mapDomande.remove((long)domanda.getId());
		db.commit();
	}
	
	/**
	 * Eliminazione di una risposta dal db
	 * @param risposta risposta da eliminare
	 */
	@Override
	public void eliminaRisposta(Risposta risposta) {
		DB db = getDB();
		if (risposta != null ) {
			Map<Long, Risposta> mapRisposte = db.hashMap(RISPOSTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
			mapRisposte.remove((long)risposta.getDomanda().getId());
			db.commit();
		}
	}
	
	/**
	 * Eliminazione di una categoria dal db
	 * @param categoria categoria da eliminare
	 */
	private void eliminaCategoria(Categoria categoria) {
		DB db = getDB();
		Map<Long, Categoria> mapCategorie = db.hashMap(CATEGORIE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapCategorie.remove((long)categoria.getNome().hashCode());
		db.commit();
	}
	
	/**
	 * Aggiornamento del nome di una categoria
	 * @param nomeVecchio vecchio nome da rimpiazzare
	 * @param nuovoNome nuovo nome 
	 */
	@Override
	public void aggiornaCategoria(String nomeVecchio, String nuovoNome) {
		Categoria categoriaOld = getCategoria(nomeVecchio);
		Categoria categoriaNew = new Categoria(nuovoNome, categoriaOld.getPadre());
		nuovaCategoria(categoriaNew);
		DB db = getDB();
		Map<Long, Categoria> mapCategorie = db.hashMap(CATEGORIE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Categoria> entry : mapCategorie.entrySet()) {
			// se la categoria è figlia di quella vecchia cambiamo il puntatore
			if (entry.getValue().getPadre() != null && entry.getValue().getPadre().equals(nomeVecchio)) {
				entry.getValue().setPadre(nuovoNome);
				mapCategorie.remove((long)entry.getValue().getNome().hashCode());
				mapCategorie.put((long)nuovoNome.hashCode(), entry.getValue());
			}
		}
		
		Map<Long, Oggetto> mapOggetti = db.hashMap(OGGETTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		Date today = new Date(); 
		for(Map.Entry<Long, Oggetto> entry : mapOggetti.entrySet()) {
			// se l'oggetto appartiene alla categoria in questione
			if (entry.getValue().getCategoria().getNome().equals(nomeVecchio)) {
				entry.getValue().setCategoria(categoriaNew);
				aggiornaOggetto(entry.getValue());
			}
		}
		
		eliminaCategoria(categoriaOld);
		db.commit();
	}
	
	/**
	 * Eliminazione di una offerta dal db
	 * @param offerta offerta da eliminare
	 */
	@Override
	public void eliminaOfferta(Offerta offerta) {
		DB db = getDB();
		Map<Long, Offerta> mapOfferte = db.hashMap(OFFERTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapOfferte.remove((long)offerta.getId());
		// se l'offerta da eliminare è la più alta devo sostituirla
		if (offerta.getOggetto().getUltimaOfferta().getId() == offerta.getId()) {
			ArrayList<Offerta> offerte = getOffertePerOggetto(offerta.getOggetto());
			// se ci sono ancora offerte per l'oggetto
			Offerta offertaMax = null;
			if (offerte.size() > 0) {
				offertaMax = offerte.get(0);
				for (Offerta element : offerte) {
					if (offertaMax.getImporto() < element.getImporto()) {
						offertaMax = element;
					}
				}
				// aggiorno l'oggetto sul dataabse mettendogli cone ultima offerta la seconda migliore
				Oggetto oggettoNew = offerta.getOggetto();
				oggettoNew.setUltimaOfferta(offertaMax);
				aggiornaOggetto(oggettoNew);
			} else {
				Oggetto oggettoNew = offerta.getOggetto();
				oggettoNew.setUltimaOfferta(null);
				aggiornaOggetto(oggettoNew);
			}
		}
		
	}
	
	/**
	 * Eliminazione di un oggetto dal db
	 * @param oggetto oggetto da eliminare
	 */
	@Override
	public void eliminaOggetto(Oggetto oggetto) {
		DB db = getDB();
		
		// rimozione dell'oggetto 
		HTreeMap<Long, Oggetto> mapOggetti = db.hashMap(OGGETTI_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		mapOggetti.remove((long)oggetto.getId());
		
		// rimozione di tutte le domande ed eventuali risposte
		Map<Long, Domanda> mapDomande = db.hashMap(DOMANDE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Domanda> entry : mapDomande.entrySet()) {

			if (oggetto.getId() == entry.getValue().getOggetto().getId()) {
				eliminaDomanda(entry.getValue());
			}		
		}
		
		// rimozione di tutte le offerte
		Map<Long, Offerta> mapOfferte = db.hashMap(OFFERTE_MAP).keySerializer(Serializer.LONG).valueSerializer(Serializer.JAVA).createOrOpen();
		for(Map.Entry<Long, Offerta> entry : mapOfferte.entrySet()) {

			if (oggetto.getId() == entry.getValue().getOggetto().getId()) {
				eliminaOfferta(entry.getValue());
			}		
		}
	}

}
