package com.objectwebsite.test;

import java.util.Date;
import org.junit.Test;
import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.objectwebsite.client.service.*;
import com.objectwebsite.client.model.*;

public class ServiceImplementorTest extends GWTTestCase {

	private ServiceAsync serviceAsync;
	private ServiceDefTarget endpoint;

	@Override
	public String getModuleName() {
		return "com.objectwebsite.ObjectWebsite";
	}

	private void setUpServer() {
		this.serviceAsync = GWT.create(Service.class);
		endpoint = (ServiceDefTarget) this.serviceAsync;
		endpoint.setServiceEntryPoint(GWT.getModuleBaseURL() + "service"); 
	}


	@Test
	public void testRegistration() {
		setUpServer();
		UtenteRegistrato utente = new UtenteRegistrato("nicknameTest", "password", "nome", "cognome", 340293482,
				"clarcj92e383q", "via di test", "test@test.com");
		serviceAsync.registration(utente, new TrueCallBack());
		serviceAsync.registration(utente, new FalseCallBack());
		
	}
	
	@Test
	public void testGetUser() {
		setUpServer();
		serviceAsync.getUser("nicknameTest", new UserCallback());
	}
	
	@Test
	public void testLogin() {
		setUpServer();
		serviceAsync.login("nicknameTest", "password", new UserCallback());
		serviceAsync.login("nicknameTest2", "password2", new NullCallback());
	}
	
	@Test
	public void testNuovoOggetto() {
		setUpServer();
		UtenteRegistrato venditore = new UtenteRegistrato("nicknameTest", "password", "nome", "cognome", 340293482,
				"clarcj92e383q", "via di test", "test@test.com");
		Oggetto oggetto = new Oggetto("Tosaerba", "Blu", (double) 5, new Categoria("Test", ""), new Date(), venditore, new Date().hashCode());
		serviceAsync.nuovoOggetto(oggetto, new NuovoOggettoCallback());
		serviceAsync.login("nicknameTest2", "password2", new NullCallback());
	}
	
	
	@Test
	public void testGetOggetti() {
		setUpServer();
		UtenteRegistrato venditore = new UtenteRegistrato("nicknameTest", "password", "nome", "cognome", 340293482,
				"clarcj92e383q", "via di test", "test@test.com");
		Oggetto oggetto = new Oggetto("Tosaerba", "Blu", (double) 5, new Categoria("Test", ""), new Date(), venditore, new Date().hashCode());
		serviceAsync.nuovoOggetto(oggetto, new NuovoOggettoCallback());
		Oggetto oggetto2 = new Oggetto("rastrello", "rosso", (double) 59, new Categoria("Test", ""), new Date(), venditore, new Date().hashCode());
		serviceAsync.nuovoOggetto(oggetto2, new NuovoOggettoCallback());
		serviceAsync.getOggetti(new GetOggettiCallback());
		
	}
	
	@Test
	public void testAggiornaOggetto() {
		setUpServer();
		UtenteRegistrato venditore = new UtenteRegistrato("nicknameTest", "password", "nome", "cognome", 340293482,
				"clarcj92e383q", "via di test", "test@test.com");
		Oggetto oggetto = new Oggetto("Radiolina", "Blu", (double) 5, new Categoria("Test", ""), new Date(), venditore, new Date().hashCode());
		serviceAsync.nuovoOggetto(oggetto, new NuovoOggettoCallback());
		oggetto.setDescrizione("Disponibile solo in colorazione verde");
		serviceAsync.aggiornaOggetto(oggetto, new AggiornaOggettoCallback());
		
	}
	
	
	
	
	
	
	private class TrueCallBack implements AsyncCallback<Boolean> {
		
		@Override
		public void onFailure(Throwable caught) {
			fail("Error: " + caught.getMessage());	
		}

		@Override
		public void onSuccess(Boolean result) {
			assertTrue("Success", result);
			finishTest();
		}
	}
	
	private class FalseCallBack implements AsyncCallback<Boolean> {

		@Override
		public void onFailure(Throwable caught) {
			fail("Error: " + caught.getMessage());	
		}

		@Override
		public void onSuccess(Boolean result) {
			assertFalse("Error", result);
			finishTest();
		}
	}
	
	private class UserCallback implements AsyncCallback<UtenteRegistrato> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(UtenteRegistrato result) {
			assertTrue("E' di tipo utente", result instanceof UtenteRegistrato);
			finishTest();
			
		}
	}
	
	private class NullCallback implements AsyncCallback<UtenteRegistrato> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(UtenteRegistrato result) {
			assertNull("Null: ", result);
			finishTest();
		}
	}
	
	private class NuovoOggettoCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertTrue(result instanceof Object);
			finishTest();

		}
	}
	
	private class GetOggettiCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertTrue(result instanceof Object);
			finishTest();

		}
	}
	
	private class AggiornaOggettoCallback implements AsyncCallback {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertTrue(result instanceof Object);
			finishTest();

		}
	}
	
}